package com.example.appdev.model
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "Comments")
class Comment (
    val idTask: String,
    val comment: String,
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
): Serializable