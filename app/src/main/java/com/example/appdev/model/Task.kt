package com.example.appdev.model
import androidx.room.*
import java.io.Serializable
import java.util.*

@Entity(tableName = "Tasks")
class Task (
    val title: String,
    val description: String,
    var orderDate: Long,
    var dateCreate: String?,
    var dateEnd: String?,
    var active: Boolean,
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
): Serializable

