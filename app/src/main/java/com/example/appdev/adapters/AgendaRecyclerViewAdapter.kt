package com.example.appdev.adapters

import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.view.LayoutInflater
import android.widget.Adapter
import android.widget.AdapterView
import android.widget.CheckBox
import com.example.appdev.R
import androidx.recyclerview.widget.RecyclerView
import com.example.appdev.model.Task
import com.example.appdev.network.AppDatabase

class AgendaRecyclerViewAdapter(private val list: List<Task>):RecyclerView.Adapter<AgendaRecyclerViewAdapter.AgendaViewHolder>(){


    var onItemClick: ((Task) -> Unit)? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AgendaViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.model_item_tarea, parent, false)
        return AgendaViewHolder(itemView)
    }

    var delegate: AgendaListener? = null

    override fun onBindViewHolder(holder: AgendaViewHolder, position: Int) {
        val currentItem = list[position]
        holder.bind(currentItem)
    }

    interface AgendaListener {
        fun setActive(position: Int,task: Task)

    }

    override fun getItemCount() = list.size



    inner class AgendaViewHolder(itemView: View): RecyclerView.ViewHolder(itemView),AdapterView.OnItemSelectedListener  {

        val number: TextView = itemView.findViewById(R.id.tv_number)
        val title: TextView = itemView.findViewById(R.id.tv_title)
        val description: TextView = itemView.findViewById(R.id.tv_description)
        val datestart: TextView = itemView.findViewById(R.id.tv_datecreate)
        val dateend: TextView = itemView.findViewById(R.id.tv_dateend)
        val checkActive: CheckBox = itemView.findViewById(R.id.chk_active)


        init {
            checkActive.setOnClickListener{
                changeActive()
            }

            itemView.setOnClickListener {
                onItemClick?.invoke(list[adapterPosition])
            }


        }

        fun bind(item: Task){
            number.text = "${item.id}"

            description.text = item.description
            title.text = item.title

            datestart.text = "Creada : ${item.dateCreate}"
            dateend.text = "Finaliza : ${item.dateEnd}"

            checkActive.isChecked = !item.active

        }
        fun changeActive(){
            val task = list[adapterPosition]
            delegate!!.setActive(adapterPosition,task)

            //Log.e("xxxadapterPosition","${list[adapterPosition].active}")

        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

        }

        override fun onNothingSelected(p0: AdapterView<*>?) {

        }
    }


}