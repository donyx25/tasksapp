package com.example.appdev.adapters

import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.view.LayoutInflater
import android.widget.Adapter
import android.widget.AdapterView
import android.widget.CheckBox
import com.example.appdev.R
import androidx.recyclerview.widget.RecyclerView
import com.example.appdev.model.Comment
import com.example.appdev.model.Task
import com.example.appdev.network.AppDatabase

class CommentRecyclerViewAdapter(private val list: List<Comment>):RecyclerView.Adapter<CommentRecyclerViewAdapter.CommentViewHolder>(){


    var onItemClick: ((Comment) -> Unit)? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.model_item_comment, parent, false)
        return CommentViewHolder(itemView)
    }


    override fun onBindViewHolder(holder: CommentViewHolder, position: Int) {
        val currentItem = list[position]
        holder.bind(currentItem)
    }



    override fun getItemCount() = list.size



    inner class CommentViewHolder(itemView: View): RecyclerView.ViewHolder(itemView),AdapterView.OnItemSelectedListener  {


        val description: TextView = itemView.findViewById(R.id.tv_description_comment)




        fun bind(item: Comment){
            description.text = item.comment
        }


        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

        }

        override fun onNothingSelected(p0: AdapterView<*>?) {

        }
    }


}