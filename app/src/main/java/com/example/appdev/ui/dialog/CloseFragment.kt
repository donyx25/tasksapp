package com.example.appdev.ui.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.example.appdev.R
import com.example.appdev.model.Task
import com.example.appdev.network.AppDatabase
import kotlinx.android.synthetic.main.fragment_close.*
import kotlinx.android.synthetic.main.fragment_task.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CloseFragment : DialogFragment() {

    private lateinit var database: AppDatabase
    var task: Task? = null
    var countComents: Int? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_close, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
    }

    fun initUI(){

        database = AppDatabase.getDatabase(activity!!)

        edt_title.setText("Seguro quieres eliminar ${task!!.title}?")

        btnCancelarClose.setOnClickListener{
            this.dismiss()
        }

        btnConfirmar.setOnClickListener{



            CoroutineScope(Dispatchers.IO).launch {
                database.tasks().delete(task!!)

                if (countComents!! > 0){
                    database.comments().deleteAll(task!!.id)
                }

            }

            activity!!.finish()





        }

    }


}