package com.example.appdev.ui.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.example.appdev.R
import com.example.appdev.model.Task
import com.example.appdev.network.AppDatabase
import kotlinx.android.synthetic.main.fragment_task.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import java.text.SimpleDateFormat

class TaskFragment : DialogFragment() {

    var id: Int? = null
    private lateinit var database: AppDatabase
    private lateinit var taskLiveData: LiveData<Task>
    private lateinit var task: Task


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_task, container, false)

    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
    }

    fun initUI(){

        database = AppDatabase.getDatabase(activity!!)



        if (id != null){

            //Log.e("xxxActive ","${id}")
            taskLiveData = database.tasks().get(id!!)

            taskLiveData.observe(this, Observer {
                task = it
                title_input.setText("${task.title}")
                description.setText(task.description)
                dateend.setText("${task.dateEnd}")

            })
        }




        btnCancelar.setOnClickListener{
            this.dismiss()
        }


        btnAceptar.setOnClickListener{
            val titulo =  title_input.text.toString()
            val description =  description.text.toString()

            val sdf = SimpleDateFormat("dd/MM")
            val currentDate = sdf.format(java.util.Date())

            val _sdf = SimpleDateFormat("yyMMddHHmmss")
            val _currentDate = _sdf.format(java.util.Date())

            val orderdate = _currentDate.toLong()
            Log.e("xxxNumber","${orderdate}")


            val fechastart =  currentDate
            val fechaend =  dateend.text.toString()

            val task = Task(titulo,description,orderdate,fechastart,fechaend,true)

            if (id != null) {
                CoroutineScope(Dispatchers.IO).launch {
                    task.id = id as Int

                    database.tasks().update(task)

                    //this.getActivity().finish();
                }
            } else {
                CoroutineScope(Dispatchers.IO).launch {
                    database.tasks().insertAll(task)

                    //this.getActivity().finish();
                }
            }
            this.dismiss()

        }

    }


}