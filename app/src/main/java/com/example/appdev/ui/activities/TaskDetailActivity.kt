package com.example.appdev.ui.activities

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.annotation.RequiresApi
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.appdev.R
import com.example.appdev.adapters.CommentRecyclerViewAdapter
import com.example.appdev.model.Comment
import com.example.appdev.model.Task
import com.example.appdev.network.AppDatabase
import com.example.appdev.ui.dialog.CloseFragment
import com.example.appdev.ui.dialog.TaskFragment
import kotlinx.android.synthetic.main.activity_agenda.*
import kotlinx.android.synthetic.main.activity_task_detail.*
import kotlinx.android.synthetic.main.activity_task_detail.toolbar_title
import kotlinx.android.synthetic.main.model_item_comment.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TaskDetailActivity : AppCompatActivity() {

    var id: Int? = null
    var title: String? = null
    private lateinit var database: AppDatabase
    private lateinit var taskLiveData: LiveData<Task>
    var task: Task? = null

    private var adapterComment: CommentRecyclerViewAdapter? = null
    private var listComments = emptyList<Comment>()

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task_detail)
        initUI()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun initUI(){
        database = AppDatabase.getDatabase(this)

        id = intent.getIntExtra("id", 0)
        //Log.e("xxxActive ","${id}")
        taskLiveData = database.tasks().get(id!!)



        rv_comments.layoutManager = LinearLayoutManager(this@TaskDetailActivity)
        rv_comments.setHasFixedSize(true)

        searchComments(id!!)




        taskLiveData.observe(this, Observer {
            task = it
            toolbar_title.text = task!!.title
            title = task!!.title
            tv_datecreate.text = "Creada : ${task!!.dateCreate}"
            tv_dateend.text = "Finaliza : ${task!!.dateEnd}"
            tv_description.text = task!!.description

        })


        imgSend.setOnClickListener {

            val description = tv_comments_input.text.toString()
            val com = Comment("${id}",description)

            Log.e("xxxID","${id}")
            Log.e("xxxdescription","${description}")

            createComment(com)
            tv_comments_input.setText("")
        }



        imgEdit.setOnClickListener {
            val dialog = TaskFragment()
            dialog.id = id
            dialog.show(supportFragmentManager, "TaskFragment")
        }

        imgTrash.setOnClickListener {

            taskLiveData.removeObservers(this)
            val dialog = CloseFragment()
            dialog.task = task!!
            dialog.countComents = this.listComments.size
            //dialog.title = title

            dialog.show(supportFragmentManager, "CloseFragment")
        }


    }

    fun createComment(comment: Comment){


        CoroutineScope(Dispatchers.IO).launch {
            database.comments().insertAll(comment)

        }
    }


    fun searchComments(idTask: Int){

        Log.e("xxxComment","${id}")

        database.comments().getComments(idTask).removeObserver { listComments }

        database.comments().getComments(idTask).observe(this, Observer {
            this.listComments = it

            if(it.isNotEmpty()) {
                tv_title_comment.visibility = View.VISIBLE
                tv_title_comment.text = "Comentarios (${it.size})"

            }
            val adapter = CommentRecyclerViewAdapter(listComments)
            Log.e("xxxSize","${it.size}")

            rv_comments.adapter = adapter
        })

        adapterComment?.notifyDataSetChanged()
        rv_comments.visibility = View.VISIBLE
    }
}