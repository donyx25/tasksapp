package com.example.appdev.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.appdev.R
import com.example.appdev.adapters.AgendaRecyclerViewAdapter
import com.example.appdev.model.Task
import com.example.appdev.network.AppDatabase
import com.example.appdev.ui.activities.TaskDetailActivity
import com.example.appdev.ui.dialog.TaskFragment
import kotlinx.android.synthetic.main.activity_agenda.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class MainActivity : AppCompatActivity(),AgendaRecyclerViewAdapter.AgendaListener {

    private lateinit var database: AppDatabase
    private var adapterAgenda: AgendaRecyclerViewAdapter? = null
    //private var listTareas = mutableListOf<TareaBean>()
    private lateinit var task: Task

    private var activeStatus: Boolean = true
    private var listaTasks = emptyList<Task>()

    private lateinit var tasksLiveData: LiveData<Task>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_agenda)


        initUI()
    }

    private fun initUI() {


        rv_agenda_all.layoutManager = LinearLayoutManager(this@MainActivity)
        rv_agenda_all.setHasFixedSize(true)


        database = AppDatabase.getDatabase(this)

        searchDatabase(activeStatus)

        imageButton.setOnClickListener {
            activeStatus = !activeStatus
            if (activeStatus){
                imageButton.setImageResource(R.drawable.ic_eye)
                searchDatabase(activeStatus)
            }
            else{
                imageButton.setImageResource(R.drawable.ic_eye_off)
                searchDatabase(activeStatus)
            }


        }


        adapterAgenda?.notifyDataSetChanged()
        rv_agenda_all.visibility = View.VISIBLE


        btn_fab.setOnClickListener{
            showNoticeDialog()
        }





    }

    fun searchDatabase(active: Boolean){
        database.tasks().getAll().removeObservers(this)


        database.tasks().getAll().observe(this, Observer {
            this.listaTasks = it.filter { it.active == this.activeStatus }
            val adapter = AgendaRecyclerViewAdapter(listaTasks)
            adapter!!.delegate = this@MainActivity

            adapter!!.onItemClick = {
                //val dialog = TaskFragment()
                //dialog.id = it.id
                //dialog.show(supportFragmentManager, "TaskFragment")
                val intent = Intent(this, TaskDetailActivity::class.java)
                intent.putExtra("id", it.id)
                startActivity(intent)
            }

            rv_agenda_all.adapter = adapter
        })


    }

    fun showNoticeDialog() {
        // Create an instance of the dialog fragment and show it
        val dialog = TaskFragment()

        dialog.show(supportFragmentManager, "TaskFragment")
        //dialog.heig

    }

    override fun setActive(position: Int, task: Task) {
        //TODO("Not yet implemented")
        var _task = task
        _task.active = !task.active
        //Log.e("xxxActive ","${activeStatus}")

        CoroutineScope(Dispatchers.IO).launch {
            database.tasks().update(_task)
        }
        //


    }


}