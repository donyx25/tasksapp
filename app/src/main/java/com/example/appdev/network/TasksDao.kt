package com.example.appdev.network

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.appdev.model.Task


@Dao
interface TasksDao {

    @Query("SELECT * FROM tasks order by orderDate desc")
    fun getAll(): LiveData<List<Task>>

    @Query("SELECT * FROM tasks WHERE id = :id")
    fun get(id: Int): LiveData<Task>

    @Insert
    fun insertAll(vararg tasks: Task)

    @Update
    fun update(task: Task)

    @Delete
    fun delete(task: Task)
}