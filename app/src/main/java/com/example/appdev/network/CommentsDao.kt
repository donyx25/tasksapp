package com.example.appdev.network

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.appdev.model.Comment
import com.example.appdev.model.Task


@Dao
interface CommentsDao {

    @Query("SELECT * FROM comments")
    fun getAll(): LiveData<List<Comment>>


    @Query("SELECT * FROM comments WHERE idTask = :idTask")
    fun getComments(idTask: Int): LiveData<List<Comment>>

    @Query("SELECT * FROM comments WHERE id = :id")
    fun get(id: Int): LiveData<Comment>

    @Query("DELETE FROM comments WHERE idTask = :idTask")
    fun deleteAll(idTask: Int)

    @Insert
    fun insertAll(vararg comments: Comment)

    @Update
    fun update(comment: Comment)

    @Delete
    fun delete(comment: Comment)
}