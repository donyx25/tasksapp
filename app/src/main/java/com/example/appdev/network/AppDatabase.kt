package com.example.appdev.network

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.appdev.model.Comment
import com.example.appdev.model.Task


@Database(entities = [Task::class,Comment::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun tasks(): TasksDao
    abstract fun comments(): CommentsDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            val tempInstance = INSTANCE

            if (tempInstance != null) {
                return tempInstance
            }

            synchronized(this) {
                val instance = Room.databaseBuilder(
                        context.applicationContext,
                    AppDatabase::class.java,
                    "AppDev"
                ).build()

                INSTANCE = instance

                return instance
            }
        }
    }
}